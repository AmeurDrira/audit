# Application Audit (Springboot)



### Requirement 

* Maven 3.8.5
* JDK 11
* [ Postgres DataBase 12.11 ](https://www.enterprisedb.com/postgresql-tutorial-resources-training?uuid=03b13357-9281-4985-baea-17b72a0febc3&campaignId=7012J000001F5IIQA0)
* add postgres to path(environment variable)
* NB (default password of postgres in our case is : root)

### DataBase Initialisation:

 * init dataBase user
```
   psql -U postgres -d postgres -a -f init_database.sql
```
* init data in dataBase
```
   psql -U audit -d audit -a -f data.sql
```
* drop all database 
```
  psql -U postgres -d postgres -a -f drop_database.sql
```


### Swagger 2 UI :

 [ Swagger ](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/)
 
### Audit Application :

* POST questions : 
    url  http://localhost:9090/api/rapport/enregister-rapport
    Body example 
```
  [
      {
      "auditId": "PXX1",
      "questionId": "5.1.1",
      "note": 4
      },
      {
      "auditId": "PXX1",
      "questionId": "5.1.2",
      "note": 3
      }
 ]
```

* GET Audit with audit Id : PXX
    url http://localhost:9090/api/rapport/audit/PXX1
    example resualt
```
{
  "audit": {
    "id": "PXX1",
    "nom": "audit génerale",
    "date": "2022-05-28",
    "societe": {
      "id": "SOC_3",
      "nom": "MTD",
      "matricule": "0048/MM/999"
    }
  },
  "chapitre": [
    {
      "idChapitre": "5",
      "chapitre": "Politiques de sécurité de l’information",
      "evalChapitre": 3.67,
      "souschapitre": [
        {
          "idSousChapitre": "5.1",
          "sousChapitre": "Orientations de la direction en matière de sécurité de l’information",
          "poids": 2,
          "evalSouschapitre": 3.67,
          "questions": [
            {
              "idQuestion": "5.1.1",
              "question": "Document de politique de sécurité de l\"information",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "5.1.2",
              "question": "Revue des politiques de sécurité de l’information",
              "note": 3,
              "poids": 2
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "6",
      "chapitre": "Organisation de la sécurité de l’information",
      "evalChapitre": 3.73,
      "souschapitre": [
        {
          "idSousChapitre": "6.1",
          "sousChapitre": "Organisation interne",
          "poids": 5,
          "evalSouschapitre": 3.75,
          "questions": [
            {
              "idQuestion": "6.1.1",
              "question": "Fonctions et responsabilités liées à la sécurité de l’information",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "6.1.2",
              "question": "séparation des tâches",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "6.1.3",
              "question": "Relations avec les autorités",
              "note": 4,
              "poids": 2
            },
            {
              "idQuestion": "6.1.4",
              "question": "Relations avec des groupes de travail spécialisés",
              "note": 4,
              "poids": 2
            },
            {
              "idQuestion": "6.1.5",
              "question": "La sécurité de l’information dans la gestion de projet",
              "note": 3,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "6.2",
          "sousChapitre": "Appareils mobiles et télétravail",
          "poids": 2,
          "evalSouschapitre": 3.67,
          "questions": [
            {
              "idQuestion": "6.2.1",
              "question": "Politique en matière d’appareils mobiles",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "6.2.2",
              "question": "Télétravail",
              "note": 4,
              "poids": 4
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "7",
      "chapitre": "La sécurité des ressources humaines",
      "evalChapitre": 3.67,
      "souschapitre": [
        {
          "idSousChapitre": "7.3",
          "sousChapitre": "Rupture, terme ou modification du contrat de travail",
          "poids": 1,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "7.3.1",
              "question": "Achèvement ou modification des responsabilités associées au contrat de travail",
              "note": 3,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "7.1",
          "sousChapitre": "Avant l’embauche",
          "poids": 2,
          "evalSouschapitre": 4,
          "questions": [
            {
              "idQuestion": "7.1.1",
              "question": "Sélection des candidats",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "7.1.2",
              "question": "Termes et conditions d’embauche",
              "note": 4,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "7.2",
          "sousChapitre": "Pendant la durée du contrat",
          "poids": 3,
          "evalSouschapitre": 3.67,
          "questions": [
            {
              "idQuestion": "7.2.1",
              "question": "Responsabilités de la direction",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "7.2.2",
              "question": "Sensibilisation, apprentissage et formation à la sécurité de l’information",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "7.2.3",
              "question": "Processus disciplinaire",
              "note": 4,
              "poids": 4
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "8",
      "chapitre": "Gestion des actifs",
      "evalChapitre": 3.26,
      "souschapitre": [
        {
          "idSousChapitre": "8.3",
          "sousChapitre": "Manipulation des supports",
          "poids": 3,
          "evalSouschapitre": 3.4,
          "questions": [
            {
              "idQuestion": "8.3.1",
              "question": "Gestion des supports amovibles",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "8.3.2",
              "question": "Mise au rebut des supports",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "8.3.3",
              "question": "Transfert physique des supports",
              "note": 3,
              "poids": 3
            }
          ]
        },
        {
          "idSousChapitre": "8.1",
          "sousChapitre": "Responsabilités relatives aux actifs",
          "poids": 4,
          "evalSouschapitre": 3.36,
          "questions": [
            {
              "idQuestion": "8.1.1",
              "question": "Inventaire des actifs",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "8.1.2",
              "question": "Propriété des actifs",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "8.1.3",
              "question": "Utilisation correcte des actifs",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "8.1.4",
              "question": "Restitution des actifs",
              "note": 3,
              "poids": 2
            }
          ]
        },
        {
          "idSousChapitre": "8.2",
          "sousChapitre": "Classification de l’information",
          "poids": 3,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "8.2.1",
              "question": "Classification des informations",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "8.2.2",
              "question": "Marquage des informations",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "8.2.3",
              "question": "Manipulation des actifs",
              "note": 3,
              "poids": 3
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "9",
      "chapitre": "Contrôle d’accès",
      "evalChapitre": 2.94,
      "souschapitre": [
        {
          "idSousChapitre": "9.3",
          "sousChapitre": "Responsabilités des utilisateurs",
          "poids": 1,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "9.3.1",
              "question": "Utilisation d’informations secrètes d’authentification",
              "note": 3,
              "poids": 3
            }
          ]
        },
        {
          "idSousChapitre": "9.1",
          "sousChapitre": "Exigences métier en matière de contrôle d’accès",
          "poids": 2,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "9.1.1",
              "question": "Politique de contrôle d’accès",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "9.1.2",
              "question": "Accès aux réseaux et aux services en réseau",
              "note": 3,
              "poids": 3
            }
          ]
        },
        {
          "idSousChapitre": "9.4",
          "sousChapitre": "Contrôle de l’accès au système et aux applications",
          "poids": 5,
          "evalSouschapitre": 2.82,
          "questions": [
            {
              "idQuestion": "9.4.1",
              "question": "Restriction d’accès à l’information",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "9.4.2",
              "question": "Sécuriser les procédures de connexion",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "9.4.3",
              "question": "Système de gestion des mots de passe",
              "note": 2,
              "poids": 3
            },
            {
              "idQuestion": "9.4.4",
              "question": "Utilisation de programmes utilitaires à privilèges",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "9.4.5",
              "question": "Contrôle d’accès au code source des programmes",
              "note": 3,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "9.2",
          "sousChapitre": "Gestion de l’accès utilisateur",
          "poids": 6,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "9.2.1",
              "question": "Enregistrement et désinscription des utilisateurs",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "9.2.2",
              "question": "Maîtrise de la gestion des accès utilisateur",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "9.2.3",
              "question": "Gestion des privilèges d’accès",
              "note": 2,
              "poids": 4
            },
            {
              "idQuestion": "9.2.4",
              "question": "Gestion des informations secrètes d’authentification des utilisateurs",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "9.2.5",
              "question": "Revue des droits d’accès utilisateur",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "9.2.6",
              "question": "Suppression ou adaptation des droits d’accès",
              "note": 3,
              "poids": 3
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "10",
      "chapitre": "Cryptographie",
      "evalChapitre": 3.5,
      "souschapitre": [
        {
          "idSousChapitre": "10.1",
          "sousChapitre": "Mesures cryptographiques",
          "poids": 2,
          "evalSouschapitre": 3.5,
          "questions": [
            {
              "idQuestion": "10.1.1",
              "question": "Politique d’utilisation des mesures cryptographiques",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "10.1.2",
              "question": "Gestion des clés",
              "note": 3,
              "poids": 4
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "11",
      "chapitre": "Sécurité physique et environnementale",
      "evalChapitre": 3.12,
      "souschapitre": [
        {
          "idSousChapitre": "11.2",
          "sousChapitre": "Matériels",
          "poids": 9,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "11.2.1",
              "question": "Emplacement et protection du matériel",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "11.2.2",
              "question": "Services généraux",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "11.2.3",
              "question": "Sécurité du câblage",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "11.2.4",
              "question": "Maintenance du matériel",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "11.2.5",
              "question": "Sortie des actifs",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "11.2.6",
              "question": "Sécurité du matériel et des actifs hors des locaux",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "11.2.7",
              "question": "Mise au rebut ou recyclage sécurisé(e) du matériel",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "11.2.8",
              "question": "Matériel utilisateur laissé sans surveillance",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "11.2.9",
              "question": "Politique du bureau propre et de l’écran vide",
              "note": 3,
              "poids": 2
            }
          ]
        },
        {
          "idSousChapitre": "11.1",
          "sousChapitre": "Zones sécurisées",
          "poids": 6,
          "evalSouschapitre": 3.3,
          "questions": [
            {
              "idQuestion": "11.1.1",
              "question": "Périmètre de sécurité physique",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "11.1.2",
              "question": "Contrôles physiques des accès",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "11.1.3",
              "question": "Sécurisation des bureaux, des salles et des équipements",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "11.1.4",
              "question": "Protection contre les menaces extérieures et environnementales",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "11.1.5",
              "question": "Travail dans les zones sécurisées",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "11.1.6",
              "question": "Zones de livraison et de chargement",
              "note": 4,
              "poids": 2
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "12",
      "chapitre": "Sécurité liée à l’exploitation",
      "evalChapitre": 3.42,
      "souschapitre": [
        {
          "idSousChapitre": "12.7",
          "sousChapitre": "Considérations sur l’audit du système d’information",
          "poids": 1,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "12.7.1",
              "question": "Mesures relatives à l’audit des systèmes d’information",
              "note": 3,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "12.3",
          "sousChapitre": "Sauvegarde",
          "poids": 1,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "12.3.1",
              "question": "Sauvegarde des informations",
              "note": 3,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "12.1",
          "sousChapitre": "Procédures et responsabilités liées à l’exploitation",
          "poids": 4,
          "evalSouschapitre": 3.64,
          "questions": [
            {
              "idQuestion": "12.1.1",
              "question": "Procédures d’exploitation documentées",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "12.1.2",
              "question": "Gestion des changements",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "12.1.4",
              "question": "Séparation des environnements de développement, de test et d’exploitation",
              "note": 4,
              "poids": 3
            }
          ]
        },
        {
          "idSousChapitre": "12.4",
          "sousChapitre": "Journalisation et surveillance",
          "poids": 4,
          "evalSouschapitre": 3.29,
          "questions": [
            {
              "idQuestion": "12.4.1",
              "question": "Journalisation des événements",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "12.4.2",
              "question": "Protection de l’information journalisée",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "12.4.3",
              "question": "Journaux administrateur et opérateur",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "12.4.4",
              "question": "Synchronisation des horloges",
              "note": 3,
              "poids": 3
            }
          ]
        },
        {
          "idSousChapitre": "12.6",
          "sousChapitre": "Gestion des vulnérabilités techniques",
          "poids": 2,
          "evalSouschapitre": 3.57,
          "questions": [
            {
              "idQuestion": "12.6.1",
              "question": "Gestion des vulnérabilités techniques",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "12.6.2",
              "question": "Restrictions liées à l’installation de logiciels",
              "note": 3,
              "poids": 3
            }
          ]
        },
        {
          "idSousChapitre": "12.2",
          "sousChapitre": "Protection contre les logiciels malveillants",
          "poids": 1,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "12.2.1",
              "question": "Mesures contre les logiciels malveillants",
              "note": 3,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "12.5",
          "sousChapitre": "Maîtrise des logiciels en exploitation",
          "poids": 1,
          "evalSouschapitre": 4,
          "questions": [
            {
              "idQuestion": "12.5.1",
              "question": "Installation de logiciels sur des systèmes en exploitation",
              "note": 4,
              "poids": 4
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "13",
      "chapitre": "Sécurité des communications",
      "evalChapitre": 3.14,
      "souschapitre": [
        {
          "idSousChapitre": "13.2",
          "sousChapitre": "Transfert de l’information",
          "poids": 4,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "13.2.1",
              "question": "Politiques et procédures de transfert de l’information",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "13.2.2",
              "question": "Accords en matière de transfert d’information",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "13.2.3",
              "question": "Messagerie électronique",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "13.2.4",
              "question": "Engagements de confidentialité ou de non-divulgation",
              "note": 3,
              "poids": 3
            }
          ]
        },
        {
          "idSousChapitre": "13.1",
          "sousChapitre": "Management de la sécurité des réseaux",
          "poids": 3,
          "evalSouschapitre": 3.33,
          "questions": [
            {
              "idQuestion": "13.1.1",
              "question": "Contrôle des réseaux",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "13.1.2",
              "question": "Sécurité des services de réseau",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "13.1.3",
              "question": "Cloisonnement des réseaux",
              "note": 4,
              "poids": 4
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "14",
      "chapitre": "Acquisition, développement et maintenance des systèmes d’information",
      "evalChapitre": 3.21,
      "souschapitre": [
        {
          "idSousChapitre": "14.2",
          "sousChapitre": "Sécurité des processus de développement et d’assistance technique",
          "poids": 9,
          "evalSouschapitre": 3.3,
          "questions": [
            {
              "idQuestion": "14.2.1",
              "question": "Politique de développement sécurisé",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "14.2.2",
              "question": "Procédures de contrôle des changements apportés au système",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "14.2.3",
              "question": "Revue technique des applications après changement apporté à la plateforme d’exploitation",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "14.2.4",
              "question": "Restrictions relatives aux changements apportés aux progiciels",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "14.2.5",
              "question": "Principes d’ingénierie de la sécurité des systèmes",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "14.2.6",
              "question": "Environnement de développement sécurisé",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "14.2.7",
              "question": "Développement externalisé",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "14.2.8",
              "question": "Phase de test de la sécurité du système",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "14.2.9",
              "question": "Test de conformité du système",
              "note": 3,
              "poids": 2
            }
          ]
        },
        {
          "idSousChapitre": "14.3",
          "sousChapitre": "Données de test",
          "poids": 1,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "14.3.1",
              "question": "Protection des données de test",
              "note": 3,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "14.1",
          "sousChapitre": "Exigences de sécurité applicables aux systèmes d’information",
          "poids": 3,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "14.1.1",
              "question": "Analyse et spécification des exigences de sécurité de l’information",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "14.1.2",
              "question": "Sécurisation des services d’application sur les réseaux publics",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "14.1.3",
              "question": "Protection des transactions liées aux services d’application",
              "note": 3,
              "poids": 4
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "15",
      "chapitre": "Relations avec les fournisseurs",
      "evalChapitre": 3.23,
      "souschapitre": [
        {
          "idSousChapitre": "15.1",
          "sousChapitre": "Sécurité de l’information dans les relations avec les fournisseurs",
          "poids": 3,
          "evalSouschapitre": 3.38,
          "questions": [
            {
              "idQuestion": "15.1.1",
              "question": "Politique de sécurité de l’information dans les relations avec les fournisseurs",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "15.1.2",
              "question": "La sécurité dans les accords conclus avec les fournisseurs",
              "note": 4,
              "poids": 3
            },
            {
              "idQuestion": "15.1.3",
              "question": "Chaine d’approvisionnement informatique",
              "note": 3,
              "poids": 2
            }
          ]
        },
        {
          "idSousChapitre": "15.2",
          "sousChapitre": "Gestion de la prestation du service",
          "poids": 2,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "15.2.1",
              "question": "Surveillance et revue des services des fournisseurs",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "15.2.2",
              "question": "Gestion des changements apportés dans les services des fournisseurs",
              "note": 3,
              "poids": 2
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "16",
      "chapitre": "Gestion des incidents liés à la sécurité de l’information",
      "evalChapitre": 3.19,
      "souschapitre": [
        {
          "idSousChapitre": "16.1",
          "sousChapitre": "Gestion des incidents liés à la sécurité de l’information et améliorations",
          "poids": 7,
          "evalSouschapitre": 3.19,
          "questions": [
            {
              "idQuestion": "16.1.1",
              "question": "Responsabilités et procédures",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "16.1.2",
              "question": "Signalement des événements liés à la sécurité de l’information",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "16.1.3",
              "question": "Signalement des failles liées à la sécurité de l’information",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "16.1.4",
              "question": "Appréciation des événements liés à la sécurité de l’information et prise de décision",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "16.1.5",
              "question": "Réponse aux incidents liés à la sécurité de l’information",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "16.1.6",
              "question": "Tirer des enseignements des incidents liés à la sécurité de l’information",
              "note": 3,
              "poids": 2
            },
            {
              "idQuestion": "16.1.7",
              "question": "Recueil de preuves",
              "note": 3,
              "poids": 2
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "17",
      "chapitre": "Aspects de la sécurité de l’information dans la gestion de la continuité de l’activité",
      "evalChapitre": 3.5,
      "souschapitre": [
        {
          "idSousChapitre": "17.2",
          "sousChapitre": "Redondances",
          "poids": 1,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "17.2.1",
              "question": "Disponibilité moyens de traitement de l’information",
              "note": 3,
              "poids": 4
            }
          ]
        },
        {
          "idSousChapitre": "17.1",
          "sousChapitre": "Continuité de la sécurité de l’information",
          "poids": 3,
          "evalSouschapitre": 3.67,
          "questions": [
            {
              "idQuestion": "17.1.1",
              "question": "Organisation de la continuité de la sécurité de l’information",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "17.1.2",
              "question": "Mise en œuvre de la continuité de la sécurité de l’information",
              "note": 4,
              "poids": 4
            },
            {
              "idQuestion": "17.1.3",
              "question": "Vérifier, revoir et évaluer la continuité de la sécurité de l’information",
              "note": 3,
              "poids": 4
            }
          ]
        }
      ]
    },
    {
      "idChapitre": "18",
      "chapitre": "Conformité",
      "evalChapitre": 3.12,
      "souschapitre": [
        {
          "idSousChapitre": "18.2",
          "sousChapitre": "Revue de la sécurité de l’information",
          "poids": 3,
          "evalSouschapitre": 3.33,
          "questions": [
            {
              "idQuestion": "18.2.1",
              "question": "Revue indépendante de la sécurité de l’information",
              "note": 4,
              "poids": 3
            },
            {
              "idQuestion": "18.2.2",
              "question": "Conformité avec les politiques et les normes de sécurité",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "18.2.3",
              "question": "Vérification de la conformité technique",
              "note": 3,
              "poids": 3
            }
          ]
        },
        {
          "idSousChapitre": "18.1",
          "sousChapitre": "Conformité aux obligations légales et réglementaires",
          "poids": 5,
          "evalSouschapitre": 3,
          "questions": [
            {
              "idQuestion": "18.1.1",
              "question": "Identification de la législation et des exigences contractuelles applicables",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "18.1.2",
              "question": "Droits de propriété intellectuelle",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "18.1.3",
              "question": "Protection des enregistrements",
              "note": 3,
              "poids": 3
            },
            {
              "idQuestion": "18.1.4",
              "question": "Protection de la vie privée et protection des données à caractère personnel",
              "note": 3,
              "poids": 4
            },
            {
              "idQuestion": "18.1.5",
              "question": "Réglementation relative aux mesures cryptographiques",
              "note": 3,
              "poids": 3
            }
          ]
        }
      ]
    }
  ]
}
```