package com.tn.audit.conf;

import com.tn.audit.model.Utilisateur;
import com.tn.audit.service.UtilisateurService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorizationUserDetailsService
        implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {
    private final UtilisateurService utilisateurService;

    /**
     * Récupère l'utilisateur à partir de la base de données et crée un objet UserDetails. *
     *
     * <p>Si l'utilisateur n'est pas trouvé, renvoie une exception
     *
     * @param token instance de PreAuthenticatedAuthenticationToken
     * @return UserDetails object contenant les informations de rôle de l'utilisateur
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token)
            throws UsernameNotFoundException {
        final String principal = (String) token.getPrincipal();
        Utilisateur user = utilisateurService.findById(principal).orElse(null);
        if (user != null) {

            return new CetiUserDetails(principal, user.getProfil(), Optional.ofNullable(user));
        }
        throw new UsernameNotFoundException("Utilisateur non trouvé");
    }
}
