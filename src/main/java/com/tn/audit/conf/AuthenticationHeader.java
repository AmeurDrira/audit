package com.tn.audit.conf;

public class AuthenticationHeader {

    public static final String X_USER_NAME = "X-USER";

    private AuthenticationHeader() {
    }
}
