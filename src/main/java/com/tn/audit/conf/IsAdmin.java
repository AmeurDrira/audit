package com.tn.audit.conf;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAuthority('ADMIN')")
public @interface IsAdmin {
    // Dommage qu'on ne puisse pas utiliser un enum dans l'annotation PreAuthorize, seules les
    // constantes sont autorisées et un enum n'est pas une constantes ='(
}
