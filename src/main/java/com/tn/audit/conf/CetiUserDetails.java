package com.tn.audit.conf;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class CetiUserDetails extends User {

    /**
     * Si utilisateur n'est pas présent, alors l'utilisateur courant n'est pas un utilisateur humain.
     */
    private Optional<CetiUser> utilisateur;

    public CetiUserDetails(
            String username,
            String password,
            boolean enabled,
            boolean accountNonExpired,
            boolean credentialsNonExpired,
            boolean accountNonLocked,
            Collection<? extends GrantedAuthority> authorities,
            Optional<CetiUser> utilisateur) {
        super(
                username,
                password,
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                authorities);
        this.utilisateur = utilisateur;
    }

    public CetiUserDetails(
            String username, ProfilUtilisateur profilUtilisateur, Optional<CetiUser> utilisateur) {
        this(
                username,
                "N/A",
                true,
                true,
                true,
                true,
                Collections.singletonList(new SimpleGrantedAuthority(profilUtilisateur.name())),
                utilisateur);
    }
}
