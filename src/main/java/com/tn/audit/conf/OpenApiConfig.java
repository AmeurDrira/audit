package com.tn.audit.conf;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Configuration d'Open API (Swagger).
 *
 * <p>
 * https://github.com/springdoc/springdoc-openapi
 * https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
 * https://github.com/OAI/OpenAPI-Specification/blob/3.0.1/versions/3.0.1.md
 */
@Configuration
@PropertySource(value = "classpath:swagger.yml", factory = YamlPropertySourceFactory.class)
public class OpenApiConfig {

    private final CetiOpenApiProperties cetiOpenApiProperties;

    public OpenApiConfig(CetiOpenApiProperties cetiOpenApiProperties) {
        this.cetiOpenApiProperties = cetiOpenApiProperties;
    }

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(new io.swagger.v3.oas.models.info.Info().title(cetiOpenApiProperties.getInfo().getTitle())
                        .description(cetiOpenApiProperties.getInfo().getDescription())
                        .version(cetiOpenApiProperties.getInfo().getVersion())
                        .license(new License().name("Apache 2.0").url("https://springdoc.org"))
                        .contact(new io.swagger.v3.oas.models.info.Contact()
                                .email(cetiOpenApiProperties.getInfo().getContact().getEmail())
                                .name(cetiOpenApiProperties.getInfo().getContact().getName())))
                .externalDocs(new ExternalDocumentation().description(cetiOpenApiProperties.getInfo().getDescription()))
                .components(new Components().addSecuritySchemes("Authentification",
                        new SecurityScheme().type(SecurityScheme.Type.APIKEY).name(AuthenticationHeader.X_USER_NAME)
                                .in(SecurityScheme.In.HEADER)))
                .addSecurityItem(new SecurityRequirement().addList("Authentification"));
    }

    @Bean
    public GroupedOpenApi adiOpenApi() {
        return GroupedOpenApi.builder().group(cetiOpenApiProperties.getInfo().getApi().getGroup())
                .addOpenApiCustomiser(customerGlobalHeaderOpenApiCustomiser()).build();
    }

    @Bean
    public OpenApiCustomiser customerGlobalHeaderOpenApiCustomiser() {
        return openApi -> openApi.getPaths().values().stream().flatMap(pathItem -> pathItem.readOperations().stream())
                .forEach(operation -> {
                    var apiResponseUnauthorized = new ApiResponse().description("Utilisateur non authentifié").content(
                            new Content().addMediaType(org.springframework.http.MediaType.APPLICATION_JSON_VALUE,
                                    new MediaType()));

                    var apiResponseForbidden = new ApiResponse().description("Utilisateur non autorisé").content(
                            new Content().addMediaType(org.springframework.http.MediaType.APPLICATION_JSON_VALUE,
                                    new MediaType()));
                    ApiResponses apiResponses = operation.getResponses();
                    apiResponses.put("401", apiResponseUnauthorized);
                    apiResponses.put("403", apiResponseForbidden);
                });
    }
}
