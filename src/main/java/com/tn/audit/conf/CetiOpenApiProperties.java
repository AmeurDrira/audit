package com.tn.audit.conf;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "ceti.openapi")
@Getter
@Setter
public class CetiOpenApiProperties {

    private Info info = new Info();

    @Getter
    @Setter
    public static class Info {
        private Contact contact = new Contact();
        private Api api = new Api();
        private String title;
        private String description;
        private String version;
    }

    @Getter
    @Setter
    public static class Contact {
        private String name;
        private String email;
    }

    @Getter
    @Setter
    public static class Api {
        private String group;
    }
}
