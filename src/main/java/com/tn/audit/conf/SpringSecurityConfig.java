package com.tn.audit.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@Import(SecurityProblemSupport.class)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private SecurityProblemSupport problemSupport;

    @Autowired
    private AuthorizationUserDetailsService authorizationUserDetailsService;


    public UrlBasedCorsConfigurationSource corsFilter() {

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOriginPattern("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("DELETE");
        config.addAllowedMethod("PATCH");
        config.addAllowedMethod("OPTION");
        source.registerCorsConfiguration("/**", config);
        return source;
    }


    // la désactivation du csrf est configurable et uniquement pour les
    // environnement de tests
    @SuppressWarnings("java:S4502")
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // désactiver csrf pour l'environnement de dev
        http.cors().configurationSource(this.corsFilter());
        http.csrf().disable();

        http.addFilterBefore(preAuthFilter(), RequestHeaderAuthenticationFilter.class)
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(problemSupport)
                .and()
                .authorizeRequests()
                // les requêtes doivent être authentifiées
                .anyRequest()
                .authenticated();

    }

    /**
     * Crée le bean RequestHeaderAuthenticationFilter et spécifie quel entête HTTP
     * est utilisé pour récupéré l'identifiant de l'utilisateur.
     */
    @Bean
    public RequestHeaderAuthenticationFilter preAuthFilter() {
        RequestHeaderAuthenticationFilter requestHeaderAuthenticationFilter = new RequestHeaderAuthenticationFilter();
        requestHeaderAuthenticationFilter.setPrincipalRequestHeader(AuthenticationHeader.X_USER_NAME);
        requestHeaderAuthenticationFilter.setAuthenticationManager(authenticationManager());
        requestHeaderAuthenticationFilter.setExceptionIfHeaderMissing(false); // FIXME vérifier l'impact de ce booleen
        return requestHeaderAuthenticationFilter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() {
        List<AuthenticationProvider> providers = new ArrayList<>(1);
        providers.add(preAuthAuthProvider());
        return new ProviderManager(providers);
    }

    @Bean
    public PreAuthenticatedAuthenticationProvider preAuthAuthProvider() {
        var provider = new PreAuthenticatedAuthenticationProvider();
        provider.setPreAuthenticatedUserDetailsService(userDetailsServiceWrapper());
        return provider;
    }

    @Bean
    public AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> userDetailsServiceWrapper() {
        return authorizationUserDetailsService;
    }

    @Override
    public void configure(WebSecurity web) {
        // les requêtes sur actuator ne sont pas protégées car appelées uniquement par
        // le monitoring
        // interne
        web.ignoring().antMatchers(HttpMethod.GET, "/actuator", "/actuator/health", "/v3/api-docs/**",
                "/swagger-ui.html", "/swagger-ui/**");

        web.ignoring().antMatchers(HttpMethod.POST, "/api/utilisateur/check-user");
    }
}
