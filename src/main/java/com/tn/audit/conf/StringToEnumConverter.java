package com.tn.audit.conf;

import org.springframework.core.convert.converter.Converter;

public class StringToEnumConverter implements Converter<String, ProfilUtilisateur> {
    @Override
    public ProfilUtilisateur convert(String source) {
        return ProfilUtilisateur.valueOf(source.toUpperCase());
    }
}