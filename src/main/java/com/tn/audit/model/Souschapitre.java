package com.tn.audit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "souschapitre")
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Souschapitre {

    @Id
    @Column(name = "souscha_id", unique = true, nullable = false)
    private String id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "cha_id", nullable = false)
    private Chapitre chapitre;

    @Column(name = "souscha_nom", nullable = false)
    private String nom;

    @Column(name = "souscha_poids")
    private BigDecimal poids;

    @OneToMany(mappedBy = "souschapitre")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Set<Question> question;
}
