package com.tn.audit.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@ToString
@Table(name = "audit")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Audit {
	@Id
	@Column(name = "audit_id")
	private String id;

	@Column(name = "audit_nom")
	private String nom;

	@Column(name = "audit_date")
	private LocalDate date;

	@ManyToOne(optional = false)
	@JoinColumn(name = "soc_id", nullable = false)
	private Societe societe;

	@OneToMany(mappedBy = "audit")
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@JsonIgnore
	private Set<Rapport> rapport;

}
