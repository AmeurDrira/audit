package com.tn.audit.model;

import lombok.*;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class AuditDto {


    private String nom;
    private LocalDate date;
    private String societeId;


}
