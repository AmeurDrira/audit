package com.tn.audit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@ToString
@Table(name = "rapport")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Rapport {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rap_id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "audit_id", nullable = false)
	private Audit audit;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id")
	private Question question;

	@Column(name = "rap_question_note")
	private Short note;

}
