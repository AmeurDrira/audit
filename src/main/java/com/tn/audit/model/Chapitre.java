package com.tn.audit.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "chapitre")
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Chapitre {

    @Id
    @Column(name = "cha_id", unique = true, nullable = false)
    private String id;

    @Column(name = "cha_nom", nullable = false)
    private String nom;


    @OneToMany(mappedBy = "chapitre")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Set<Souschapitre> souschapitre;
}
