package com.tn.audit.model;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CheckUserDto {
    private Boolean exist;
}
