package com.tn.audit.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "question")
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Question {

  @Id
  @Column(name = "question_id", length = 10, unique = true, nullable = false)
  private String id;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "souscha_id", nullable = false)
  private Souschapitre souschapitre;

  @Column(name = "question_nom", nullable = false)
  private String nom;
  
  @Column(name = "question_poids")
  private BigDecimal poids;
  
  @OneToMany(mappedBy = "question")
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @JsonIgnore

  private List<Rapport> rapport;
  
}
