package com.tn.audit.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@ToString
@Table(name = "societe")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Societe {

	@Id
	@Column(name = "soc_id")
	private String id;

	@Column(name = "soc_nom")
	private String nom;

	@Column(name = "soc_matricule")
	private String matricule;

	@OneToMany(mappedBy = "societe")
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@JsonIgnore
	private Set<Audit> audit;

}
