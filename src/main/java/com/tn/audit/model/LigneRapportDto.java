package com.tn.audit.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor

@Getter
@Setter
@ToString

public class LigneRapportDto {


	private String auditId;

	private String questionId;

	private Short note;

}
