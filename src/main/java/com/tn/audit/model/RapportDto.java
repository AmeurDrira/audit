package com.tn.audit.model;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString

public class RapportDto {

    private Audit audit;
    private List<ChapitreDto> chapitre;

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class ChapitreDto {
        private String idChapitre;
        private String chapitre;
        private BigDecimal evalChapitre;
        private List<SouschapitreDto> souschapitre;
    }

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class SouschapitreDto {
        private String idSousChapitre;
        private String sousChapitre;
        private BigDecimal poids;
        private BigDecimal evalSouschapitre;
        private List<QuestionDto> questions;
    }

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class QuestionDto {
        private String idQuestion;
        private String question;
        private BigDecimal note;
        private BigDecimal poids;
    }


}
