package com.tn.audit.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.tn.audit.conf.CetiUser;
import com.tn.audit.conf.ProfilUtilisateur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
@Builder
@Table(name = "utilisateur")
public class Utilisateur extends CetiUser implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "uti_gaia")
	private String gaia;

	@Column(name = "uti_profil_utilisateur")
	@Enumerated(EnumType.STRING)
	private ProfilUtilisateur profil;

	@Column(name = "uti_prenom")
	private String prenom;

	@Column(name = "uti_nom")
	private String nom;

	@Column(name = "uti_mot_de_passe", nullable = false)
	private String motdepasse;

}
