package com.tn.audit.exceptions;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

@SuppressWarnings("serial")
public class FailedUpdateException extends AbstractThrowableProblem {

  public FailedUpdateException() {
    super(null, "Failed Update", Status.BAD_REQUEST, "modification échouée");
  }

  public FailedUpdateException(final String message) {
    super(null, message, Status.BAD_REQUEST, "modification échouée");
  }
}
