package com.tn.audit.exceptions;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

@SuppressWarnings("serial")
public class NotFoundException extends AbstractThrowableProblem {

    public NotFoundException() {
        super(null, "Not found", Status.NOT_FOUND, "donnée inexistante");
    }

    public NotFoundException(final String message) {
        super(null, message, Status.NOT_FOUND, "donnée inexistante");
    }
}
