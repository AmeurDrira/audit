package com.tn.audit.exceptions;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

@SuppressWarnings("serial")
public class NoContentException extends AbstractThrowableProblem {
    public NoContentException() {
        super(null, "No Content", Status.NO_CONTENT, "donnée inexistante");
    }

}
