package com.tn.audit.exceptions;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class FailedCreationException extends AbstractThrowableProblem {

	private static final long serialVersionUID = 1L;
	private static final String EXCEPTION_TITLE = "Création échouée";

	public FailedCreationException(String message) {
		super(null, EXCEPTION_TITLE, Status.BAD_REQUEST, message);
	}
}
