package com.tn.audit.rest;

import com.tn.audit.conf.IsAdmin;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NoContentException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Chapitre;
import com.tn.audit.service.ChapitreService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/chapitre")
@Tag(name = "Chapitre", description = "Endpoint exposé pour gérer les Chapitres")
@Slf4j
@RequiredArgsConstructor
@IsAdmin
public class ChapitreController {

	private final ChapitreService chapitreService;

	@GetMapping
	@Operation(summary = "Récupérer touts les chapitres", description = "Récupérer toutes les chapitres")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Chapitre.class)))) })
	public List<Chapitre> getAllChapitres() {
		log.debug("REST requête de récupération la liste des Chapitres");
		return chapitreService.findAll();
	}

	@GetMapping("/{id}")
	@Operation(summary = "Récupérer un chapitre", description = "Récupérer un chapitre")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Chapitre.class))) })
	public Chapitre getChapitreById(@PathVariable final String id) {
		log.debug("REST requête de récupération d'une chapitre : {}", id);
		return chapitreService.findById(id).orElseThrow(NoContentException::new);
	}

	@PutMapping("/{id}")
	@Operation(summary = "Modifier un chapitre", description = "Modifier un chapitre")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Chapitre.class))) })
	public Chapitre updateChapitre(@PathVariable String id, @RequestBody Chapitre patch) {
		Chapitre chapitreOld = chapitreService.findById(id).orElseThrow(NotFoundException::new);
		patch.setId(chapitreOld.getId());

		return Optional.ofNullable(chapitreService.update(patch)).orElseThrow(FailedUpdateException::new);

	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Supprimer un chapitre", description = "Supprimer un chapitre")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Chapitre.class))) })
	public void deleteChapitreById(@PathVariable final String id) {
		log.debug("REST requête de suppression d'un chapitre : {}", id);
		chapitreService.deleteById(id);
	}

	@PostMapping()
	@Operation(summary = "enregistrer un chapitre", description = "enregistrer un chapitre")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Chapitre.class))) })
	public Chapitre saveChapitre(@RequestBody Chapitre chapitre) {

		return Optional.ofNullable(chapitreService.create(chapitre)).orElseThrow(FailedUpdateException::new);

	}
}
