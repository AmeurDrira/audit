package com.tn.audit.rest;

import com.tn.audit.conf.IsAdmin;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Souschapitre;
import com.tn.audit.service.SouschapitreService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/souschapitre")
@Tag(name = "Souschapitre", description = "Endpoint exposé pour gérerles Souschapitre")
@Slf4j
@RequiredArgsConstructor
@IsAdmin
public class SouschapitreController {

    private final SouschapitreService souschapitreService;

    @GetMapping("/{id}")
    @Operation(summary = "Récupérer une souschapitre", description = "Récupérer une souschapitre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Souschapitre.class)))})
    public Souschapitre getSouschapitreById(@PathVariable final String id) {
        log.debug("REST requête de récupération d'une souschapitre : {}", id);
        return souschapitreService.findById(id).orElseThrow(NotFoundException::new);
    }

    @GetMapping
    @Operation(summary = "liste des souschapitres", description = "liste des souschapitres", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Souschapitre.class))))})
    public List<Souschapitre> getAllSouschapitre() {
        return souschapitreService.getAllSousChapitre();
    }

    @PutMapping("/{id}")
    @Operation(summary = "Modifier un souschapitre", description = "Modifier un souschapitre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Souschapitre.class)))})
    public Souschapitre updateChapitre(@PathVariable String id, @RequestBody Souschapitre patch) {
        Souschapitre souschapitreOld = souschapitreService.findById(id).orElseThrow(NotFoundException::new);
        patch.setId(souschapitreOld.getId());

        return Optional.ofNullable(souschapitreService.update(patch)).orElseThrow(FailedUpdateException::new);

    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Supprimer un souschapitre", description = "Supprimer un souschapitre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Souschapitre.class)))})
    public void deleteSouschapitreById(@PathVariable final String id) {
        log.debug("REST requête de suppression d'un souschapitre : {}", id);
        souschapitreService.deleteById(id);
    }

    @PostMapping()
    @Operation(summary = "enregistrer un souschapitre", description = "enregistrer un souschapitre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Souschapitre.class)))})
    public Souschapitre saveSouschapitre(@RequestBody Souschapitre souschapitre) {

        return Optional.ofNullable(souschapitreService.create(souschapitre)).orElseThrow(FailedUpdateException::new);

    }

}
