package com.tn.audit.rest;

import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NoContentException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.LigneRapportDto;
import com.tn.audit.model.Rapport;
import com.tn.audit.model.RapportDto;
import com.tn.audit.service.RapportService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/rapport")
@Tag(name = "Rapport", description = "Endpoint exposé pour gérer les Rapports")
@Slf4j
@RequiredArgsConstructor
public class RapportController {

    private final RapportService rapportService;

    @GetMapping
    @Operation(summary = "Récupérer touts les rapports", description = "Récupérer toutes les rapports")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Rapport.class))))})
    public List<Rapport> getAllRapports() {
        log.debug("REST requête de récupération la liste des Rapports");
        return rapportService.findAll();
    }

    @GetMapping("/{id}")

    @Operation(summary = "Récupérer un rapport", description = "Récupérer un rapport")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Rapport.class)))})
    public Rapport getRapportById(@PathVariable final Long id) {
        log.debug("REST requête de récupération d'un rapport : {}", id);
        return rapportService.findById(id).orElseThrow(NoContentException::new);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Modifier le rapport", description = "Modifier le rapport")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Rapport.class)))})

    public Rapport updateRapport(@PathVariable Long id, @RequestBody Rapport patch) {
        Rapport rapportOld = rapportService.findById(id).orElseThrow(NotFoundException::new);
        patch.setId(rapportOld.getId());

        return Optional.ofNullable(rapportService.update(patch)).orElseThrow(FailedUpdateException::new);

    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Supprimer un rapport", description = "Supprimer un rapport")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Rapport.class)))})
    public void deleteRapportById(@PathVariable final Long id) {
        log.debug("REST requête de suppression d'un rapport : {}", id);
        rapportService.deleteById(id);
    }

    @PostMapping()
    @Operation(summary = "enregistrer Une ligne dans le rapport", description = "enregistrer Une ligne dans le rapport")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = LigneRapportDto.class)))})
    public Rapport savelineRapport(@RequestBody LigneRapportDto ligneRapportDto) {

        return Optional.ofNullable(rapportService.createLineRapport(ligneRapportDto)).orElseThrow(FailedUpdateException::new);

    }

    @PostMapping("/enregister-rapport")
    @Operation(summary = "enregistrer Un rapport", description = "enregistrer un rapport")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(array = @ArraySchema(schema = @Schema(implementation = LigneRapportDto.class))))})
    public List<Rapport> savelineRapport(@RequestBody List<LigneRapportDto> rapportDto) {

        return rapportService.createRapport(rapportDto);

    }

    @GetMapping("/audit/{id}")
    @Operation(summary = "Récupérer un rapport par un audit", description = "Récupérer un rapport par un audit")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = RapportDto.class)))})
    public RapportDto getRapportByAuditId(@PathVariable final String id) {
        log.debug("REST requête de récupération d'un rapport par audit_id: {}", id);
        return rapportService.getRapportwithScore(id);
    }

}
