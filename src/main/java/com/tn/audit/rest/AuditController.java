package com.tn.audit.rest;

import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NoContentException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Audit;
import com.tn.audit.model.AuditDto;
import com.tn.audit.service.AuditService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/audit")
@Tag(name = "Audit", description = "Endpoint exposé pour gérer les Audits")
@Slf4j
@RequiredArgsConstructor
public class AuditController {

    private final AuditService auditService;

    @GetMapping
    @Operation(summary = "Récupérer touts les audits", description = "Récupérer toutes les audits")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Audit.class))))})
    public List<Audit> getAllAudits() {
        log.debug("REST requête de récupération la liste des Audits");
        return auditService.findAll();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Récupérer un audit", description = "Récupérer un audit")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Audit.class)))})
    public Audit getAuditById(@PathVariable final String id) {
        log.debug("REST requête de récupération d'un audit : {}", id);
        return auditService.findById(id).orElseThrow(NoContentException::new);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Modifier l'audit", description = "Modifier l'audit")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Audit.class)))})
    public Audit updateAudit(@PathVariable String id, @RequestBody Audit patch) {
        Audit auditOld = auditService.findById(id).orElseThrow(NotFoundException::new);
        patch.setId(auditOld.getId());

        return Optional.ofNullable(auditService.update(patch)).orElseThrow(FailedUpdateException::new);

    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Supprimer un audit", description = "Supprimer un audit")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Audit.class)))})
    public void deleteAuditById(@PathVariable final String id) {
        log.debug("REST requête de suppression d'un audit : {}", id);
        auditService.deleteById(id);
    }

    @PostMapping()
    @Operation(summary = "enregistrer un audit", description = "enregistrer un audit")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = AuditDto.class)))})
    public Audit saveAudit(@RequestBody AuditDto auditDto) {

        return Optional.ofNullable(auditService.create(auditDto)).orElseThrow(FailedUpdateException::new);

    }
}
