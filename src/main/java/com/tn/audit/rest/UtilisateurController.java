package com.tn.audit.rest;

import com.tn.audit.conf.IsAdmin;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NoContentException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.CheckUserDto;
import com.tn.audit.model.Utilisateur;
import com.tn.audit.service.UtilisateurService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.data.domain.Sort.Direction.ASC;

@RestController
@RequestMapping("/api/utilisateur")
@Tag(name = "Utilisateur", description = "Endpoint exposé pour gérer les Utilisateurs")
@Slf4j
@RequiredArgsConstructor

public class UtilisateurController {

    private final UtilisateurService utilisateurService;

    @GetMapping
    @IsAdmin
    @Operation(summary = "Récupérer touts les utilisateurs", description = "Récupérer toutes les utilisateurs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Utilisateur.class))))})
    public List<Utilisateur> getAllUtilisateurs(
            @Parameter(hidden = true) @PageableDefault(sort = {"nom"}, direction = ASC) final Pageable pageable) {
        log.debug("REST requête de récupération la liste des Utilisateurs");
        return utilisateurService.findAll();
    }

    @GetMapping("/{id}")
    @IsAdmin
    @Operation(summary = "Récupérer un utilisateur", description = "Récupérer un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Utilisateur.class)))})
    public Utilisateur getUtilisateurById(@PathVariable final String id) {
        log.debug("REST requête de récupération d'un utilisateur : {}", id);
        return utilisateurService.findById(id).orElseThrow(NoContentException::new);
    }

    @PutMapping("/{id}")
    @IsAdmin
    @Operation(summary = "Modifier le utilisateur", description = "Modifier le utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Utilisateur.class)))})
    public Utilisateur updateUtilisateur(@PathVariable String id, @RequestBody Utilisateur patch) {
        Utilisateur utilisateurOld = utilisateurService.findById(id).orElseThrow(NotFoundException::new);
        patch.setGaia(utilisateurOld.getGaia());

        return Optional.ofNullable(utilisateurService.update(patch)).orElseThrow(FailedUpdateException::new);

    }

    @DeleteMapping("/{id}")
    @IsAdmin
    @Operation(summary = "Supprimer un utilisateur", description = "Supprimer un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Utilisateur.class)))})
    public void deleteUtilisateurById(@PathVariable final String id) {
        log.debug("REST requête de suppression d'un utilisateur : {}", id);
        utilisateurService.deleteById(id);
    }

    @PostMapping()
    @IsAdmin
    @Operation(summary = "enregistrer un utilisateur", description = "enregistrer un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Utilisateur.class)))})
    public Utilisateur saveUtilisateur(@RequestBody Utilisateur utilisateur) {
        log.debug("REST requête enregistrer un utilisateur : {}", utilisateur);
        return Optional.ofNullable(utilisateurService.create(utilisateur)).orElseThrow(FailedUpdateException::new);

    }

    @PostMapping("/check-user")
    @Operation(summary = "verifier un utilisateur", description = "verifier un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = CheckUserDto.class)))})
    public CheckUserDto verifierUtilisateur(@RequestParam(name = "gaia") String gaia, @RequestParam(name = "motdepasse") String motdepasse) {

        return utilisateurService.checkUser(gaia, motdepasse);

    }
}
