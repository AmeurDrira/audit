package com.tn.audit.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tn.audit.conf.IsAdmin;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Question;
import com.tn.audit.service.QuestionService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/question")
@Tag(name = "Question", description = "Endpoint exposé pour gérer agence d'intervention (Question)")
@Slf4j
@IsAdmin
@RequiredArgsConstructor
public class QuestionController {

	private final QuestionService questionService;

	@GetMapping
	@Operation(summary = "Récupérer touts les questions", description = "Récupérer toutes les questions")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Question.class)))) })
	public List<Question> getAllQuestions() {
		log.debug("REST requête de récupération la liste des questions");
		return questionService.findAll();
	}

	@GetMapping("/{id}")
	@Operation(summary = "Récupérer une Question ", description = "Récupérer une Question")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Question.class))) })
	public Question getAiById(@PathVariable final String id) {
		log.debug("REST requête de récupération d'une Question : {}", id);
		return questionService.findById(id).orElseThrow(NotFoundException::new);
	}

	@PutMapping("/{id}")
	@Operation(summary = "Modifier une Question", description = "Modifier une Question")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Question.class))) })
	@IsAdmin
	public Question updateChapitre(@PathVariable String id, @RequestBody Question patch) {
		Question questionOld = questionService.findById(id).orElseThrow(NotFoundException::new);
		patch.setId(questionOld.getId());

		return Optional.ofNullable(questionService.update(patch)).orElseThrow(FailedUpdateException::new);

	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Supprimer une Question", description = "Supprimer une Question")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Question.class))) })
	public void deleteQuestionById(@PathVariable final String id) {
		log.debug("REST requête de suppression d'une Question : {}", id);
		questionService.deleteById(id);
	}
	
	@PostMapping()
	@Operation(summary = "enregistrer une Question", description = "enregistrer une Question")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Question.class))) })
	public Question saveQuestion(@RequestBody Question question) {

		return Optional.ofNullable(questionService.create(question)).orElseThrow(FailedUpdateException::new);

	}
}
