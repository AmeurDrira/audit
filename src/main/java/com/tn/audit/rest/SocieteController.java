package com.tn.audit.rest;

import com.tn.audit.conf.IsAdmin;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NoContentException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Societe;
import com.tn.audit.service.SocieteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/societe")
@Tag(name = "Societe", description = "Endpoint exposé pour gérer les Societes")
@Slf4j
@RequiredArgsConstructor
@IsAdmin
public class SocieteController {

    private final SocieteService societeService;

    @GetMapping
    @Operation(summary = "Récupérer touts les societes", description = "Récupérer toutes les societes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Societe.class))))})
    public List<Societe> getAllSocietes() {
        log.debug("REST requête de récupération la liste des Societes");
        return societeService.findAll();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Récupérer une societe", description = "Récupérer une societe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Societe.class)))})
    public Societe getSocieteById(@PathVariable final String id) {
        log.debug("REST requête de récupération d'une societe : {}", id);
        return societeService.findById(id).orElseThrow(NoContentException::new);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Modifier une societe", description = "Modifier une societe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Societe.class)))})
    public Societe updateSociete(@PathVariable String id, @RequestBody Societe patch) {
        Societe societeOld = societeService.findById(id).orElseThrow(NotFoundException::new);
        patch.setId(societeOld.getId());

        return Optional.ofNullable(societeService.update(patch)).orElseThrow(FailedUpdateException::new);

    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Supprimer une societe", description = "Supprimer une societe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Societe.class)))})
    public void deleteSocieteById(@PathVariable final String id) {
        log.debug("REST requête de suppression d'un societe : {}", id);
        societeService.deleteById(id);
    }

    @PostMapping()
    @Operation(summary = "enregistrer une societe", description = "enregistrer une societe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Opération réussie", content = @Content(schema = @Schema(implementation = Societe.class)))})
    public Societe saveSociete(@RequestBody Societe societe) {

        return Optional.ofNullable(societeService.create(societe)).orElseThrow(FailedUpdateException::new);

    }
}
