package com.tn.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tn.audit.model.Chapitre;

public interface ChapitreRepository extends JpaRepository<Chapitre, String> {}
