package com.tn.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tn.audit.model.Societe;

public interface SocieteRepository extends JpaRepository<Societe, String> {}
