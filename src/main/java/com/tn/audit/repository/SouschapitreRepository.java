package com.tn.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tn.audit.model.Souschapitre;

public interface SouschapitreRepository extends JpaRepository<Souschapitre, String> {

}
