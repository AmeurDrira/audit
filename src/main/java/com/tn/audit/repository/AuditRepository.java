package com.tn.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tn.audit.model.Audit;

public interface AuditRepository extends JpaRepository<Audit, String> {
}
