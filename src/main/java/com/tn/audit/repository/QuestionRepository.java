package com.tn.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tn.audit.model.Question;

public interface QuestionRepository extends JpaRepository<Question, String> {

}
