package com.tn.audit.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tn.audit.model.Rapport;

public interface RapportRepository extends JpaRepository<Rapport, Long> {

    @Query(value = "SELECT * FROM rapport r " + "WHERE r.audit_id = :auditId "
            + "AND r.question_id = :questionId ", nativeQuery = true)
    Optional<Rapport> findLineRapportByAuditIdAndQuestion(@Param("questionId") String questionId,
                                                          @Param("auditId") String auditId);

    @Query(value = "SELECT * FROM rapport r " + "WHERE r.audit_id = :auditId ", nativeQuery = true)
    List<Rapport> findRapportByAuditId(
            @Param("auditId") String auditId);

}
