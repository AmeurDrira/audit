package com.tn.audit.repository;

import com.tn.audit.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UtilisateurRepository
        extends JpaRepository<Utilisateur, String> {


    @Query(value = "SELECT * FROM utilisateur u " + "WHERE u.uti_gaia = :gaia "
            + "AND u.uti_mot_de_passe = :motdepasse ", nativeQuery = true)
    Optional<Utilisateur> findUserWithGaiaAndPassword(@Param("gaia") String gaia,
                                                      @Param("motdepasse") String motdepasse);
}
