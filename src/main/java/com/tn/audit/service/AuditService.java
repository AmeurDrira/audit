package com.tn.audit.service;

import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Audit;
import com.tn.audit.model.AuditDto;
import com.tn.audit.model.Societe;
import com.tn.audit.repository.AuditRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class AuditService {

    private final AuditRepository auditRepository;
    private final SocieteService societeService;

    @Transactional(readOnly = true)
    public List<Audit> findAll() {
        log.debug("Récupération des toutes les Audits");
        return auditRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Audit> findById(final String id) {
        log.debug("Récupération du Audit in service : {}", id);
        return auditRepository.findById(id);
    }

    public Audit update(Audit audit) {
        log.debug("modifier un audit dans le service : {}", audit.getId());
        final Optional<Audit> oldRegion = findById(audit.getId());
        if (oldRegion.isEmpty()) {
            throw new NotFoundException(String.format("Le Audit %s n’existe pas.", audit.getId()));
        }
        return auditRepository.save(audit);
    }

    private void verifyAudit(final String id) throws NotFoundException {
        log.debug("verifier un audit dans le service : {}", id);
        if (findById(id).isEmpty()) {
            log.error("Le Audit {} n’existe pas.", id);
            throw new NotFoundException(String.format("Le Audit %s n’existe pas.", id));
        }
    }

    public void deleteById(final String id) {
        log.debug("supprimer un audit dans le service : {}", id);
        try {
            verifyAudit(id);
        } catch (NotFoundException e) {
            throw new FailedUpdateException(e.getMessage());
        }

        auditRepository.deleteById(id);
    }

    public Audit create(final AuditDto auditDto) {
        log.debug("cree un audit dans le service : {}", auditDto);
        final Societe societe = Societe.builder()
                .id(auditDto.getSocieteId()).nom(auditDto.getNom())
                .build();
        final Societe societeSaved = societeService.createIfNotExist(societe);
        final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        final Audit audit = Audit.builder()
                .societe(societeSaved)
                .date(auditDto.getDate())
                .id(String.valueOf(timestamp.getTime()))
                .build();

        return auditRepository.save(audit);
    }
}
