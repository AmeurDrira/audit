package com.tn.audit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tn.audit.exceptions.FailedCreationException;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Question;
import com.tn.audit.repository.QuestionRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class QuestionService {

    private final QuestionRepository questionRepository;

    @Transactional(readOnly = true)
    public List<Question> findAll() {
        log.debug("Récupération des toutes les questions");
        return questionRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Question> findById(final String id) {
        log.debug("Récupération question in service : {}", id);
        return questionRepository.findById(id);
    }

    public Question update(Question question) {
        log.debug("modifier une question dans le service : {}", question.getId());
        try {
            verifyQuestion(question);
        } catch (NotFoundException e) {
            throw new FailedUpdateException(e.getMessage());
        }

        return questionRepository.save(question);
    }

    private void verifyQuestion(Question question) throws NotFoundException {
        log.debug("verifier une question dans le service : {}", question.getId());
        if (findById(question.getId()).isEmpty()) {
            log.error("La question {} n’existe pas.", question.getId());
            throw new NotFoundException(String.format("La question %s n’existe pas.", question.getId()));
        }
    }

    private void verifyQuestion(final String id) throws NotFoundException {
        log.debug("verifier une question dans le service : {}", id);
        if (findById(id).isEmpty()) {
            log.error("La question {} n’existe pas.", id);
            throw new NotFoundException(String.format("La question %s n’existe pas.", id));
        }
    }

    public void deleteById(final String id) {
        log.debug("supprimer une question dans le service : {}", id);
        try {
            verifyQuestion(id);
        } catch (NotFoundException e) {
            throw new FailedUpdateException(e.getMessage());
        }

        questionRepository.deleteById(id);
    }

    public Question create(Question question) {
        log.debug("cree une question dans le service : {}", question.getId());
        Optional<Question> alreadyExistsQuestion = findById(question.getId());
        if (alreadyExistsQuestion.isPresent()) {
            throw new FailedCreationException(
                    String.format("la societe %s existe déjà en base de données", question.getId()));
        }

        return questionRepository.save(question);
    }

}
