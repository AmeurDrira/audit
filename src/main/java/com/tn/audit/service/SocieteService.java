package com.tn.audit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tn.audit.exceptions.FailedCreationException;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Societe;
import com.tn.audit.repository.SocieteRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class SocieteService {

    private final SocieteRepository societeRepository;

    @Transactional(readOnly = true)
    public List<Societe> findAll() {
        log.debug("Récupération des toutes les Societes");
        return societeRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Societe> findById(final String id) {
        log.debug("Récupération du Societe in service : {}", id);
        return societeRepository.findById(id);
    }

    public Societe update(Societe societe) {
        log.debug("modifier une societe avec id : {}", societe.getId());
        final Optional<Societe> oldSociete = findById(societe.getId());
        if (oldSociete.isEmpty()) {
            throw new NotFoundException(String.format("La Societe %s n’existe pas.", societe.getId()));
        }
        return societeRepository.save(societe);
    }

    private void verifySociete(final String id) throws NotFoundException {
        log.debug("verifier une societe avec id : {}", id);
        if (findById(id).isEmpty()) {
            log.error("La Societe {} n’existe pas.", id);
            throw new NotFoundException(String.format("La Societe %s n’existe pas.", id));
        }
    }

    public void deleteById(final String id) {
        log.debug("supprimer une societe avec id : {}", id);
        try {
            verifySociete(id);
        } catch (NotFoundException e) {
            throw new FailedUpdateException(e.getMessage());
        }

        societeRepository.deleteById(id);
    }

    public Societe create(Societe societe) {
        log.debug("cree une societe avec id : {}", societe.getId());
        final Optional<Societe> alreadyExistsSociete = findById(societe.getId());
        if (alreadyExistsSociete.isPresent()) {
            throw new FailedCreationException(
                    String.format("la societe %s existe déjà en base de données", societe.getId()));
        }

        return createIfNotExist(societe);
    }

    public Societe createIfNotExist(Societe societe) {
        log.debug("cree une societe si n'exsite pas avec id : {}", societe.getId());
        final Optional<Societe> alreadyExistsSociete = findById(societe.getId());
        return alreadyExistsSociete.orElseGet(() -> societeRepository.save(societe));
    }
}
