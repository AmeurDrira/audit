package com.tn.audit.service;

import java.util.List;
import java.util.Optional;

import com.tn.audit.model.CheckUserDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tn.audit.exceptions.FailedCreationException;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Utilisateur;
import com.tn.audit.repository.UtilisateurRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class UtilisateurService {

    private final UtilisateurRepository utilisateurRepository;

    @Transactional(readOnly = true)
    public List<Utilisateur> findAll() {
        log.debug("Récupération des tous les Utilisateurs");
        return utilisateurRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Utilisateur> findById(String id) {
        log.debug("findById un Utilisateur {}", id);
        return utilisateurRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public CheckUserDto checkUser(String gaia, String motdepasse) {
        log.debug("checkUser un Utilisateur gaia : {} , motdepasse : {}", gaia, motdepasse);
        final CheckUserDto checkUserDto = new CheckUserDto();
        final Optional<Utilisateur> alreadyExistsUser = utilisateurRepository.findUserWithGaiaAndPassword(gaia, motdepasse);
        checkUserDto.setExist(alreadyExistsUser.isPresent());
        return checkUserDto;
    }

    public Utilisateur create(Utilisateur utilisateur) {
        log.debug("cree un utilisateur {}",utilisateur);
        final Optional<Utilisateur> alreadyExistsUser = utilisateurRepository.findById(utilisateur.getGaia());
        if (alreadyExistsUser.isPresent()) {
            throw new FailedCreationException(
                    String.format("l'utilisateur %s existe déjà en base de données", utilisateur.getGaia()));
        }

        return utilisateurRepository.save(utilisateur);
    }

    public Utilisateur update(Utilisateur utilisateur) {
        log.debug("modifier un utilisateur {}", utilisateur.getGaia());
        try {
            checkUpdateUtilisateur(utilisateur);
        } catch (NotFoundException e) {
            throw new NotFoundException(e.getMessage());
        }
        return utilisateurRepository.save(utilisateur);
    }

    private void checkUpdateUtilisateur(Utilisateur utilisateur) throws NotFoundException {
        log.debug("check pour modifier un Utilisateur avec un utilisateur{}", utilisateur.getGaia());
        final Optional<Utilisateur> oldUser = findById(utilisateur.getGaia());
        if (oldUser.isEmpty()) {
            log.error("Le profil utilisateur {} n’existe pas.", utilisateur.getGaia());
            throw new NotFoundException(String.format("Le profil utilisateur %s n’existe pas.", utilisateur.getGaia()));
        }
    }

    private void checkUpdateUtilisateur(String gaia) throws NotFoundException {
        log.debug("check pour modifier un Utilisateur avec un id {}", gaia);
        final Optional<Utilisateur> oldUser = findById(gaia);
        if (oldUser.isEmpty()) {
            log.error("Le profil utilisateur {} n’existe pas.", gaia);
            throw new NotFoundException(String.format("Le profil utilisateur %s n’existe pas.", gaia));
        }
    }

    public void deleteById(final String id) {
        log.debug("supprimer un utilisateur {}", id);
        try {
            checkUpdateUtilisateur(id);
        } catch (NotFoundException e) {
            throw new FailedUpdateException(e.getMessage());
        }

        utilisateurRepository.deleteById(id);
    }

}
