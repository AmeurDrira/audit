package com.tn.audit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tn.audit.exceptions.FailedCreationException;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Souschapitre;
import com.tn.audit.repository.SouschapitreRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class SouschapitreService {

    private final SouschapitreRepository sousChapitreRepository;

    @Transactional(readOnly = true)
    public Optional<Souschapitre> findById(final String id) {
        log.debug("Récupération du SousChapitre in service : {}", id);
        return sousChapitreRepository.findById(id);
    }

    public Souschapitre update(final Souschapitre sousChapitre) {
        log.debug("modifier un SousChapitre in service : {}", sousChapitre.getId());
        verifySousChapitre(sousChapitre);

        return sousChapitreRepository.save(sousChapitre);
    }

    private void verifySousChapitre(final Souschapitre sousChapitre) {
        if (findById(sousChapitre.getId()).isEmpty()) {
            log.error("Le SousChapitre {} n’existe pas.", sousChapitre.getId());
            throw new NotFoundException(String.format("Le SousChapitre %s n’existe pas.", sousChapitre.getId()));
        }
    }

    private void verifySousChapitre(final String id) {
        if (findById(id).isEmpty()) {
            log.error("Le SousChapitre {} n’existe pas.", id);
            throw new NotFoundException(String.format("Le SousChapitre %s n’existe pas.", id));
        }
    }

    @Transactional(readOnly = true)
    public List<Souschapitre> getAllSousChapitre() {
        log.debug("Récupération tous les SousChapitre in service ");
        return sousChapitreRepository.findAll();
    }

    public void deleteById(final String id) {
        log.debug("supprimer un SousChapitre in service {}", id);
        try {
            verifySousChapitre(id);
        } catch (NotFoundException e) {
            throw new FailedUpdateException(e.getMessage());
        }

        sousChapitreRepository.deleteById(id);
    }

    public Souschapitre create(Souschapitre souschapitre) {
        log.debug("creation un SousChapitre in service {}", souschapitre.getId());
        final Optional<Souschapitre> alreadyExistsSouschapitre = findById(souschapitre.getId());
        if (alreadyExistsSouschapitre.isPresent()) {
            throw new FailedCreationException(
                    String.format("le souschapitre %s existe déjà en base de données", souschapitre.getId()));
        }

        return sousChapitreRepository.save(souschapitre);
    }
}
