package com.tn.audit.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.tn.audit.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tn.audit.exceptions.FailedCreationException;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.repository.RapportRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class RapportService {

    private final RapportRepository rapportRepository;
    private final AuditService auditService;
    private final QuestionService questionService;

    @Transactional(readOnly = true)
    public List<Rapport> findAll() {
        log.debug("Récupération des toutes les Rapports");
        return rapportRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Rapport> findById(final Long id) {
        log.debug("Récupération du Rapport in service : {}", id);
        return rapportRepository.findById(id);
    }

    public Rapport update(Rapport rapport) {
        log.debug("modifier un Rapport in service : {}", rapport.getId());
        Optional<Rapport> oldRapport = findById(rapport.getId());
        if (oldRapport.isEmpty()) {
            throw new NotFoundException(String.format("Le Rapport %s n’existe pas.", rapport.getId()));
        }
        return rapportRepository.save(rapport);
    }

    private void verifyRapport(final Long id) throws NotFoundException {
        log.debug("verifier un Rapport in service : {}", id);
        if (findById(id).isEmpty()) {
            log.error("Le Rapport {} n’existe pas.", id);
            throw new NotFoundException(String.format("Le Rapport %s n’existe pas.", id));
        }
    }

    public void deleteById(final Long id) {
        log.debug("supprimer un Rapport in service : {}", id);
        try {
            verifyRapport(id);
        } catch (NotFoundException e) {
            throw new FailedUpdateException(e.getMessage());
        }

        rapportRepository.deleteById(id);
    }

    public Rapport createLineRapport(final LigneRapportDto ligneRapport) {
        log.debug("cree une ligne dans le rapport in service : {}", ligneRapport);
        Optional<Rapport> alreadyExistsLigneRapport = rapportRepository.findLineRapportByAuditIdAndQuestion(ligneRapport.getQuestionId(), ligneRapport.getAuditId());
        if (alreadyExistsLigneRapport.isPresent()) {
            throw new FailedCreationException(String.format("la ligne de rapport du gestion %s et audit %s existe déjà en base de données", ligneRapport.getQuestionId(), ligneRapport.getAuditId()));
        }

        final Audit audit = auditService.findById(ligneRapport.getAuditId()).orElseThrow(() -> new NotFoundException(String.format("Le Audit %s n’existe pas.", ligneRapport.getAuditId())));
        final Question question = questionService.findById(ligneRapport.getQuestionId()).orElseThrow(() -> new NotFoundException(String.format("La question %s n’existe pas.", ligneRapport.getQuestionId())));

        Rapport rapport = Rapport.builder().audit(audit).question(question).note(ligneRapport.getNote())

                .build();

        return rapportRepository.save(rapport);
    }

    public List<Rapport> createRapport(final List<LigneRapportDto> rapport) {
        log.debug("cree un rapport in service rapport.size : {}", rapport.size());
        final List<Rapport> listRapportCreated = new ArrayList<>();
        for (LigneRapportDto item : rapport) {
            listRapportCreated.add(createLineRapport(item));
        }

        return listRapportCreated;
    }

    public RapportDto getRapportwithScore(final String auditId) {
        log.debug("Récupérer un rapport de l'audite {} rempli dans service avec ", auditId);
        final Audit audit = auditService.findById(auditId).orElseThrow(() -> new NotFoundException(String.format("Le Audit %s n’existe pas.", auditId)));
        RapportDto rapportDto = RapportDto.builder().audit(audit).build();
        final List<Rapport> listRapport = rapportRepository.findRapportByAuditId(auditId);
        final List<Chapitre> listChapitre = listRapport.stream().map(Rapport::getQuestion).map(Question::getSouschapitre).map(Souschapitre::getChapitre).distinct().collect(Collectors.toList());
        List<RapportDto.ChapitreDto> chapitreDtoList = new ArrayList<>();
        for (Chapitre chapitre : listChapitre) {

            BigDecimal rateChapitre = BigDecimal.ZERO;
            BigDecimal sousChapitrePoids = BigDecimal.ZERO;
            List<RapportDto.SouschapitreDto> souschapitreDtoList = new ArrayList<>();
            for (Souschapitre souschapitre : chapitre.getSouschapitre()) {
                List<Rapport> questionList = listRapport.stream().filter(rapport -> rapport.getQuestion().getSouschapitre().getId().equals(souschapitre.getId())).collect(Collectors.toList());
                BigDecimal rate = BigDecimal.ZERO;
                BigDecimal sommePoids = BigDecimal.ZERO;
                final List<RapportDto.QuestionDto> questionDtoList = new ArrayList<>();
                for (Rapport r : questionList) {
                    RapportDto.QuestionDto questionDto = RapportDto.QuestionDto.builder()
                            .idQuestion(r.getQuestion().getId()).poids(r.getQuestion().getPoids())
                            .question(r.getQuestion().getNom())
                            .note(BigDecimal.valueOf(r.getNote())).build();

                    rate = rate.add(r.getQuestion().getPoids().multiply(BigDecimal.valueOf(r.getNote())));
                    sommePoids = sommePoids.add(r.getQuestion().getPoids());
                    questionDtoList.add(questionDto);
                }
                BigDecimal rateSouschapitre = rate.divide(sommePoids, 2, RoundingMode.HALF_UP);
                log.debug("{} souschapitre  eval {}", souschapitre.getId(), rateSouschapitre);
                sousChapitrePoids = sousChapitrePoids.add(souschapitre.getPoids());
                rateChapitre = rateChapitre.add(rateSouschapitre.multiply(souschapitre.getPoids()));

                RapportDto.SouschapitreDto souschapitreDto = RapportDto.SouschapitreDto.builder().questions(questionDtoList).idSousChapitre(souschapitre.getId()).sousChapitre(souschapitre.getNom()).poids(souschapitre.getPoids()).evalSouschapitre(rateSouschapitre).build();
                souschapitreDtoList.add(souschapitreDto);
            }
            BigDecimal evalChapitre = rateChapitre.divide(sousChapitrePoids, 2, RoundingMode.HALF_UP);
            log.debug("{} Chapitre  eval {}", chapitre.getId(), evalChapitre);
            RapportDto.ChapitreDto chapitreDto = RapportDto.ChapitreDto.builder().idChapitre(chapitre.getId()).chapitre(chapitre.getNom()).evalChapitre(evalChapitre).souschapitre(souschapitreDtoList).build();
            chapitreDtoList.add(chapitreDto);
        }
        rapportDto.setChapitre(chapitreDtoList);


        return rapportDto;
    }
}
