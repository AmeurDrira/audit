package com.tn.audit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tn.audit.exceptions.FailedCreationException;
import com.tn.audit.exceptions.FailedUpdateException;
import com.tn.audit.exceptions.NotFoundException;
import com.tn.audit.model.Chapitre;
import com.tn.audit.repository.ChapitreRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class ChapitreService {

    private final ChapitreRepository chapitreRepository;

    @Transactional(readOnly = true)
    public List<Chapitre> findAll() {
        log.debug("Récupération des toutes les Chapitre");
        return chapitreRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Chapitre> findById(final String id) {
        log.debug("Récupération du Chapitre in service : {}", id);
        return chapitreRepository.findById(id);
    }

    public Chapitre update(Chapitre chapitre) {
        log.debug("modifier un chapitre dans le service : {}", chapitre.getId());
        final Optional<Chapitre> oldRegion = findById(chapitre.getId());
        if (oldRegion.isEmpty()) {
            throw new NotFoundException(String.format("La Chapitre %s n’existe pas.", chapitre.getId()));
        }
        return chapitreRepository.save(chapitre);
    }

    private void verifyChapitre(final String id) throws NotFoundException {
        log.debug("verifier un chapitre dans le service : {}", id);
        if (findById(id).isEmpty()) {
            log.error("La question {} n’existe pas.", id);
            throw new NotFoundException(String.format("La question %s n’existe pas.", id));
        }
    }

    public void deleteById(final String id) {
        log.debug("supprimer un chapitre dans le service : {}", id);
        try {
            verifyChapitre(id);
        } catch (NotFoundException e) {
            throw new FailedUpdateException(e.getMessage());
        }

        chapitreRepository.deleteById(id);
    }

    public Chapitre create(Chapitre chapitre) {
        log.debug("cree un chapitre dans le service : {}", chapitre.getId());
        final Optional<Chapitre> alreadyExistsChapitre = findById(chapitre.getId());
        if (alreadyExistsChapitre.isPresent()) {
            throw new FailedCreationException(
                    String.format("le chapitre %s existe déjà en base de données", chapitre.getId()));
        }

        return chapitreRepository.save(chapitre);
    }
}
