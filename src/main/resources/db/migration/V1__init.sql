create table societe
(
    soc_id  VARCHAR(255)  NOT NULL,
    soc_matricule VARCHAR(255) ,
    soc_nom VARCHAR(255) ,
    CONSTRAINT societe_pkey PRIMARY KEY (soc_id)
);

create table audit
(
    audit_id VARCHAR(255)  NOT NULL,
    audit_date date,
    audit_nom VARCHAR(255) ,
    soc_id VARCHAR(255)  NOT NULL,
    CONSTRAINT audit_pkey PRIMARY KEY (audit_id),
    CONSTRAINT fkepw4uhux6t2japppvaimjtb6d FOREIGN KEY (soc_id)
        REFERENCES societe (soc_id) on delete restrict on update restrict
);