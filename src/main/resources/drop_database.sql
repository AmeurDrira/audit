SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'audit'
  AND pid <> pg_backend_pid();


DROP DATABASE IF EXISTS audit;
DROP ROLE IF EXISTS audit;