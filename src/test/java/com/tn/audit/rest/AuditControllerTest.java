package com.tn.audit.rest;

import com.tn.audit.data.Data;
import com.tn.audit.repository.AuditRepository;
import com.tn.audit.repository.SocieteRepository;
import com.tn.audit.utils.IntegrationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@IntegrationTest
class AuditControllerTest {

    public static final String URL_AUDIT = "/api/audit";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SocieteRepository societeRepository;
    @Autowired
    private AuditRepository auditRepository;

    @BeforeEach
    void setUpAll() {
        //auditRepository.deleteAll();
        //societeRepository.deleteAll();
        societeRepository.save(Data.getSociete());
        auditRepository.save(Data.getAudit());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void return_audit_when_get_audit_by_id() throws Exception {
        final RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URL_AUDIT + "/{id}", "AUDIT_01")
                .accept(MediaType.APPLICATION_JSON);
        this.mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", equalTo("AUDIT_01")))
                .andDo(MockMvcResultHandlers.print());
    }
}