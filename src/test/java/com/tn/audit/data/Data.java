package com.tn.audit.data;

import com.tn.audit.model.Audit;
import com.tn.audit.model.Societe;

import java.time.LocalDate;

public class Data {

    public static Societe getSociete() {
        return Societe.builder()
                .id("SOC_01")
                .nom("PSA")
                .matricule("0000/48/MM")
                .build();
    }
    public static Audit getAudit() {
        return Audit.builder()
                .id("AUDIT_01")
                .date(LocalDate.of(2022,5,28))
                .nom("AUDIT TEST")
                .societe(getSociete())
                .build();
    }
}
