package com.tn.audit.config;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/** Configuration propre aux tests. */
@Configuration
public class TestConfig {

  @Autowired private DataSource dataSource;

  /**
   * S'assure qu'un clean de la base est fait avant chaque migration lors des tests.
   *
   * @return FlywayMigrationStrategy
   */
  @DependsOn("flyway")
  @Bean
  public FlywayMigrationStrategy clean() {
    return flyway -> {
      flyway.clean();
      flyway.migrate();
    };
  }

  @Primary
  @Bean
  public Flyway flyway() {
    FluentConfiguration flywayConf = new FluentConfiguration();
    flywayConf.dataSource(dataSource);
    flywayConf.mixed(true);
    return new Flyway(flywayConf);
  }
}
